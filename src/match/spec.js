const { expect } = require('chai');

const match = require('./index');
const _ = Symbol.for('_');

describe('match', () => {
  it('should be a function of length 2', () => {
    expect(match).to.have.lengthOf(2);
  });

  it('should return a value when condition is `_`', () => {
    expect(match(_, () => 5)(1)).to.equal(5);
  });

  it('should throw an error if `exec` is not a function', () => {
    const matcher = match(_, 5);

    expect(() => matcher(5)).to.throw(TypeError);
  });

  it('should return the value of the earliest matching condition', () => {
    const result = match([1, 2],  () => "first match")
                         (_,      () => "catchall");

    expect(result([1, 2])).to.equal("first match");
  });

  it('should return catchall value if nothing else matches', () => {
    const result = match([2, 3], () => "no match")
                        (_,      () => "catchall");

    expect(result([1, 2, 3])).to.equal("catchall");
  });

  describe('Comparisons', () => {
    it('should match on object subsets', () => {
      const value = { a: 1, b: 2 };

      const result = match({ a: 1 }, () => true)
      (_,        () => false)
      (value);

      expect(result).to.be.true;
    });

    it('it should work when value is an array and condition is a function', () => {
      const incl = a => as => as.indexOf(a) > -1;
      const values = [1, 2, 3];
      const matches = match(incl(3), () => true)
                           (_, () => false);

      expect(matches(values)).to.be.true;
    });


    it('it should work when value and condition are empty arrays', () => {
      const incl = a => as => as.indexOf(a) > -1;
      const values = [];
      const matches = match(incl(3), () => false)
                           ([],      () => true)
                           (_,       () => false);

      expect(matches(values)).to.be.true;
    });
  });

  describe('Value-Last approach', () => {
    it('should return a function of length 2 when receiving two input variables (cond, exec)', () => {
      expect(match(1, x => x)).to.have.lengthOf(2);
    });

    it('should return a function of length 2 when callback receives two input variables (cond, exec)', () => {
      const matches = match(1, x => x)
                           (2, x => x);

      expect(matches).to.have.lengthOf(2);
    });

    it('should not return a function when callback receives one input argument (term)', () => {
      const matched = match(1, x => x);
      expect(matched(1)).to.not.be.a('function');
    });

    it('should raise an error when no matching clauses are found', () => {
      const expected = /no matching clause/i;
      const matches = match(1, x => x)
                           (2, x => x)
                           (3, x => x);

      expect(() => matches(4)).to.throw(expected);
    });

    it('should raise an error when no clauses are provided', () => {
      const expected = /no clauses provided to match against/i;

      expect(() => match(123)).to.throw(expected);
    });

    it('should return value applied to match when matching clause is provided', () => {
      const matches = match(1, x => x + 2)
                           (2, x => x);

      expect(matches(1)).to.equal(3);
    });
  });
});
